# Codequality Report

Reference: [https://gitlab.com/gitlab-org/ci-sample-projects/codequality-report](https://gitlab.com/gitlab-org/ci-sample-projects/codequality-report)

## Prerequisites

1. [Configure gitlab runner]
- Register your own runner
- Or use gitlab saas runner (requires credit card to validate account)

2. [Create, update and commit .gitlab-ci.yml file]
```no-highlight
variables:
  TIMEOUT_SECONDS: 10

include:
  - template: Code-Quality.gitlab-ci.yml
```

## How it looks like

<img src="pipeline-run-codequality.jpg"  width="50%" height="50%">
